package ru.itbrick.testapp.models;

import java.util.Objects;

public class Car {
    private String manufacturer;
    private String model;
    private User user;
    private long price;

    public Car(String manufacturer, String model, User user, long price) {
        this.manufacturer = manufacturer;
        this.model = model;
        this.user = user;
        this.price = price;
    }

    public Car(){

    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Car car = (Car) o;
        return price == car.price &&
                Objects.equals(manufacturer, car.manufacturer) &&
                Objects.equals(model, car.model) &&
                Objects.equals(user, car.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(manufacturer, model, user, price);
    }

    @Override
    public String toString() {
        return "Car{" +
                "manufacturer='" + manufacturer + '\'' +
                ", model='" + model + '\'' +
                ", user=" + user +
                ", price=" + price +
                '}';
    }
}
